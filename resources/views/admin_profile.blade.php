@extends('admin.dashboard.layouts.main')

@section('title')
    {{ $title }}
@endsection

@section('sidebar_item')
    @include('admin.dashboard.partials.sidebar')
@endsection

@section('content')
    <div class="card p-5 mb-5 mx-5" style="margin-top: 100px;">
        <h4 class="pb-3">{{ $title }}</h4>
        <div class="row">
            <form action="{{ route('admin.profile.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @if (session('success'))
                    <div class="alert alert-success" role="alert" class="text-danger">
                        {{ session('success') }}
                    </div>
                @endif

                <!-- Kolom Pertama -->
                <div class="col-md-3" style="float: left; padding-right: 2rem">
                    <div class="card mb-4">
                        <img id="file-preview" src="{{ asset('avatars/' . (auth()->user()->avatar ?? 'default.png')) }}"
                            class="rounded-circle mx-auto mt-4" alt="Profil Picture" style="width: 150px; height: 150px;">

                        <div class="card-body text-center mt-4 mb-5 d-flex gap-5 px-3 justify-content-center">
                            {{-- <button class="btn btn-light border fw-bold"> <i data-feather="upload"></i> Pilih Foto</button> --}}
                            <label for="avatar" class="btn btn-light border fw-bold"> <i data-feather="upload"></i> Pilih
                                Foto</label>

                            <input type="file" id="avatar" name="avatar" style="display: none;"
                                accept=".jpg, .jpeg, .png" @error('avatar') is-invalid @enderror value="{{ old('avatar') }}"
                                autocomplete="avatar">

                            <button id="deleteAvatar" class="btn btn-light border fw-bold"> <i data-feather="trash-2"></i>
                                Hapus Foto</button>
                            <input type="hidden" name="delete_avatar">

                            @error('avatar')
                                <span role="alert" class="text-danger">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                </div>

                <!-- Kolom Kedua -->
                <div class="col-md-9" style="float: right;">
                    <div class="card mb-4">
                        <div class="card-header bg-white pt-3">
                            <h5>Informasi Profil</h5>
                        </div>
                        <div class="card-body">

                            <div class="mb-3">
                                <label for="nama" class="form-label">
                                    <h6>Nama</h6>
                                </label>
                                <input type="text" class="form-control" id="nama" name="nama"
                                    value="{{ auth()->user()->nama }}" autofocus="">
                                @error('nama')
                                    <span role="alert" class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="nim" class="form-label">
                                    <h6>NIM</h6>
                                </label>
                                <input type="text" class="form-control" id="nim" name="nim"
                                    value="{{ auth()->user()->nim }}" autofocus="">
                                @error('nim')
                                    <span role="alert" class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">
                                    <h6>Email</h6>
                                </label>
                                <input type="text" class="form-control" id="email" name="email"
                                    value="{{ auth()->user()->email }}" autofocus="">
                                @error('email')
                                    <span role="alert" class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="d-flex gap-3">
                                <button type="submit" class="btn btn-primary mt-3 px-5 float-end fw-bold">Simpan</button>
                                <button type="submit" class="btn btn-danger mt-3 px-5 float-end fw-bold">Batal</button>
                            </div>
                        </div>
                    </div>
            </form>


            <div class="card mb-4">
                <div class="card-header bg-white pt-3">
                    <h5>Ganti Kata Sandi</h5>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.profile.change.password') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="current_password" class="form-label">Password Saat Ini</label>
                            <input type="password" class="form-control" id="current_password" name="current_password"
                                placeholder="Masukkan Password Anda Saat Ini" required>
                        </div>
                        <div class="mb-3">
                            <label for="new_password" class="form-label">Password Baru</label>
                            <input type="password" class="form-control" id="new_password" name="new_password"
                                placeholder="Tulis Password Baru Anda" required>
                        </div>
                        <p>Kata sandi harus memiliki minimal 7 karakter, minimal 1 digit</p>
                        <div class="mb-3">
                            <label for="new_password_confirmation" class="form-label">Konfirmasi Password Baru</label>
                            <input type="password" class="form-control" id="new_password_confirmation"
                                name="new_password_confirmation" placeholder="Konfirmasi Password Baru" required>
                        </div>
                        <div class="d-flex gap-3">
                            <button type="submit" class="btn btn-primary mt-3 px-5 float-end fw-bold">Simpan</button>
                            <button type="submit" class="btn btn-danger mt-3 px-5 float-end fw-bold">Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Kolom Ketiga -->
        {{-- <div class="col-md-3">
          <div class="card mb-4">
              <div class="card-header bg-white pt-3">
                  <h5>Pemberitahuan</h5>
              </div>
              <div class="card-footer bg-white">
                  <button class="btn btn-primary mt-2" style="width: 100%"><i data-feather="bell"></i> Nyalakan</button>
              </div>
          </div>
          <div class="card mb-4">
              <div class="card-header bg-white pt-3">
                  <h5>Update</h5>
              </div>
              <div class="card-body">
                  <p class="text-danger text-center">Anda harus melakukan update profil anda sebelum melihat update"</p>
              </div>
              <div class="card-footer bg-white">
                  <button class="btn btn-primary mt-2" style="width: 100%">Update</button>
              </div>
          </div>
      </div> --}}
    </div>
    <script src="{{ asset('js/previewAvatar.js') }}"></script>
    <script src="{{ asset('js/deleteAvatar.js') }}"></script>
    </div>
@endsection
