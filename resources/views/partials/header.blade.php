<section id="header">
    <header >
        <div class="container">
            <div class="row align-items-center justify-content-center pb-3">
                <!-- Kolom pertama -->
                <div class="col-lg-6">
                    <div class="text-lg-left">
                        <h1 class="pb-3">Back To School</h1>
                        <p class="pb-3">Program "Back to school" merupakan sebuah program yang diinisiasi oleh kampus dengan tujuan mempromosikan institusi kepada sekolah-sekolah asal para mahasiswa.</p>
                        @guest
                        {{-- DISEMBUNYIKAN  --}}
                        <a href="{{ url('/signin') }}">
                            <a href="/signin" class="btn btn-dark px-4 mb-5 px-5 pt-2 pb-2" hidden>Login</a>
                        </a>
                        {{-- ----------------}}
                        @else
                        <a href="{{ url('/signin') }}">
                            <a href="/User/form" class="btn btn-dark px-4 mb-5 px-5 pt-2 pb-2 fw-bold">Daftar</a>
                        </a>
                        @endguest
                    </div>
                    <table style="width: 100%" class="mb-5">
                        <tr class="text-center">
                            <td class="display-5">1200+</td>
                            <td class="display-5">600+</td>
                        </tr>
                        <tr class="text-center text-muted">
                            <td>Mahasiswa sudah terdaftar</td>
                            <td>Sekolah sudah didatangi</td>
                        </tr>
                    </table>
                </div>
                <!-- Kolom kedua -->
                <div class="col-lg-6 mb-5">
                    <img src="https://images.unsplash.com/photo-1530099486328-e021101a494a?q=80&w=2147&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="Gambar Header" class="img-fluid">

                </div>
            </div>
        </div>
    </header>

</section>
