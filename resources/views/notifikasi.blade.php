@extends('layouts.mainform')

@section('title', 'BTS-ITSK | Notifikasi')

@section('registration')
    {{-- NAVBAR  --}}
    @include('partials.navbar')

    {{-- NOTIFIKASI  --}}
    @include('partials.notifikasi')

    {{-- FOOTER  --}}
    @include('partials.footer')
@endsection